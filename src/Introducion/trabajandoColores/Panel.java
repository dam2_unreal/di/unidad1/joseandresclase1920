package Introducion.trabajandoColores;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class Panel extends JPanel{

	public Panel(){
		
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);	
		Graphics2D g2 = (Graphics2D) g;
		g2.setPaint(new Color(50,50,200).brighter().brighter());
		Rectangle2D rect= new Rectangle2D.Double(100, 100, 200, 150);
		g2.draw(rect);
		g2.setFont(new Font("Arial", Font.BOLD, 21));
		g2.drawString("AeIoU", 50, 50);
		g2.setColor(Color.RED.darker());
		g2.fill(rect);		
	}	
}