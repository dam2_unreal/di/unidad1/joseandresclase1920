package Introducion.Ventanas;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Ventana extends JFrame{
	
	private JPanel p;

	public Ventana(){
		super("Escribiendo en un panel sobre ventana");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
		this.setBounds(50, 50, 600, 450);
		
		this.setLayout(null);
		p = new Panel();
		p.setBackground(Color.GREEN);
		p.setBounds(10, 150, 200, 150);
		this.add(p);
	}
	
}