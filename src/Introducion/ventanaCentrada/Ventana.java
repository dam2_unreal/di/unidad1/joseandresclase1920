package Introducion.ventanaCentrada;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Ventana extends JFrame{

	public Ventana(){
		super("Ventana centrada en la pantalla");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		
		//Obtener resolucion pantalla
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanyoPantalla = miPantalla.getScreenSize();
		
		int altoPantalla = tamanyoPantalla.height;
		int anchoPantalla = tamanyoPantalla.width;
		
		this.setBounds(anchoPantalla/4, altoPantalla/4, anchoPantalla/2, altoPantalla/2);
		
		//Cambiar el icono
		Image miIcono = miPantalla.getImage("src/Recursos/image1.jpg");
		this.setIconImage(miIcono);
		
	}
	
}
