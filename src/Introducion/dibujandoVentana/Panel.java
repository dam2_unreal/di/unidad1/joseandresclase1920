package Introducion.dibujandoVentana;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class Panel extends JPanel{

	public Panel(){
		
	}	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
//		g.drawString("Cabecera", 350, 25);
//		g.drawString("Pie", 650, 550);
//		g.drawRect(100, 100, 100, 100);
//		
//		g2.setFont(new Font("Arial",Font.BOLD,15));
//		g2.drawString("Usando G2", 300, 300);
//		g2.setPaint(Color.RED);
//		g2.fillRect(100, 150, 200, 250);
//		g2.setPaint(Color.YELLOW);
//		g2.fill(new Rectangle(150,200,250,300));
//		g2.setPaint(Color.RED);
//		g2.fill(new Rectangle2D.Double(200,250,300,350));
		
		Rectangle2D rectanguloUno = new Rectangle2D.Double(100,100,200,150);
		Ellipse2D elipseUno = new Ellipse2D.Double();
		Ellipse2D elipseDos = new Ellipse2D.Double();
		elipseUno.setFrame(rectanguloUno);
		
		double centroX = rectanguloUno.getCenterX();
		double centroY = rectanguloUno.getCenterY();
		elipseDos.setFrameFromCenter(centroX, centroY, centroX+125, centroY+125);
		
		g2.draw(rectanguloUno);
//		g2.draw(new Ellipse2D.Double(100,100,200,150));		
		g2.fill(elipseUno);
		g2.draw(elipseDos);
		
		g2.setColor(Color.RED);
		g2.drawLine(100, 100, 300, 250);
		
	}
}
