package Introducion.primeraVentana;

import javax.swing.JFrame;

public class Ventana extends JFrame{

	public Ventana(){
		super("Ventana");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this.setSize(400,500);
		this.setBounds(100, 200, 300, 400);
		this.setResizable(false);
		this.setExtendedState(MAXIMIZED_BOTH);
	}
	
}