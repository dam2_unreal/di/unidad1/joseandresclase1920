package Introducion.escribiendoEnVentana;

import javax.swing.JFrame;

public class Ventana extends JFrame{

	public Ventana(){
		super("Escribiendo en un panel sobre ventana");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
		this.setBounds(200, 50, 700, 600);
		
		Panel miPanel = new Panel(); //Lamina
		this.add(miPanel);
	}
	
}