package Menu.PruebaMenu;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class Ventana extends JFrame{
	
	private JMenuBar miBarra;
	private JMenu archivo, edicion, herramientas, opciones;
	private JMenuItem guardar, guardarComo, cortar, copiar, pegar, generales, opcion1, opcion2;
	
	private JPopupMenu menuEmergente;
	private JMenuItem itemEmergente1, itemEmergente2;
	private JPanel miPanel;
	
	public Ventana() {
		this.setTitle("Menu");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(300, 300, 500, 350);
		this.setResizable(false);
		
		iniciarComponentes();
		
		this.setVisible(true);
	}
	
	public void iniciarComponentes() {
		
		miBarra = new JMenuBar();
		
		//miBarra
		archivo = new JMenu("Archivo");
		edicion = new JMenu("Edicion");
		herramientas = new JMenu("Herramientas");
		opciones = new JMenu("Opciones");
		
		//Archivo
		guardar = new JMenuItem("Guardar", KeyEvent.VK_G);
		guardarComo = new JMenuItem("Guardar Como...");
		
		//Edicion
		copiar = new JMenuItem("Copiar");
		cortar = new JMenuItem("Cortar");
		pegar = new JMenuItem("Pegar");
		
		//Edicion -> Opciones
		opcion1 = new JMenuItem("Opcion 1");
		opcion2 = new JMenuItem("Opcion 2");
		
		//Herramientas
		generales = new JMenuItem("Generales");
		
		//Anyadir a Archivos
		archivo.add(guardar);
		archivo.add(guardarComo);	
		
		//Anyadir a Edicion
		edicion.add(cortar);
		edicion.add(copiar);
		edicion.add(pegar);	
		opciones.add(opcion1);
		opciones.add(opcion2);		
		
		//Anyadir a Herramientas
		herramientas.add(generales);
		
		//Anyadir al menu
		miBarra.add(archivo);
		miBarra.add(edicion);
		edicion.addSeparator();
		edicion.add(opciones);
		miBarra.add(herramientas);	
		
		setJMenuBar(miBarra); // <-JFrame || JPanel ->this.add(miBarra, BorderLayout.NORTH);	
		
		menuEmergente = new JPopupMenu();
		
		itemEmergente1 = new JMenuItem("Opcion Emergente 1");
		itemEmergente2 = new JMenuItem("Opcion Emergente 2");
		
		menuEmergente.add(itemEmergente1);
		menuEmergente.add(itemEmergente2);
		
		miPanel = new JPanel();
		miPanel.setComponentPopupMenu(menuEmergente);
		add(miPanel);
		
		//Listener
		guardar.addActionListener(e -> System.out.println("Holi"));
		
		//Blindeos
		archivo.setMnemonic(KeyEvent.VK_A);
	}
}