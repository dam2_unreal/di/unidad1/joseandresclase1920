package Eventos.EventosFoco;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Jose Andres Sanchez Perez
 * 28 oct. 2019
 */

public class Panel extends JPanel implements FocusListener{
	private JTextField campoEmail, campoComprobacion;
	private JLabel etiquetaEmail, etiquetaComprobacion;
	
	//------------------------------------------------------------------------------------------
	
	public Panel() {
		this.setLayout(null);
		
		IniciandoComponentes();		
		SituandoComponentes();
		campoEmail.addFocusListener(this);	
		AñadiendoAlPanel();
	}
	
	//------------------------------------------------------------------------------------------


	private void IniciandoComponentes() {
		etiquetaEmail = new JLabel("Email: ");
		etiquetaComprobacion = new JLabel("¿Email correcto?: ");
		campoEmail = new JTextField();
		campoComprobacion = new JTextField();
	}
	
	private void SituandoComponentes() {
		etiquetaEmail.setBounds(10, 10, 150, 20);
		etiquetaComprobacion.setBounds(10, 50, 150, 20);
		campoEmail.setBounds(160, 10, 150, 20);
		campoComprobacion.setBounds(160, 50, 150, 20);
	}
	
	private void AñadiendoAlPanel() {
		this.add(etiquetaEmail);
		this.add(etiquetaComprobacion);
		this.add(campoEmail);
		this.add(campoComprobacion);
	}	
	
	//------------------------------------------------------------------------------------------

	@Override
	public void focusGained(FocusEvent e){}
	
	@Override
	public void focusLost(FocusEvent e) {
		String email = campoEmail.getText();
		boolean comprobacion = false;
		for (int i = 0; i < email.length(); i++) {
			if (email.charAt(i) == '@') comprobacion = true;
		}
		
		campoComprobacion.setText(comprobacion?"Correcto":"Incorrecto");
	}
}