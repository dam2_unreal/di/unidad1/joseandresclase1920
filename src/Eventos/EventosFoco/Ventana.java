package Eventos.EventosFoco;

import java.awt.event.*;
import javax.swing.*;

public class Ventana extends JFrame {
	public Ventana() {
		this.setTitle("Ventana Respondiendo FocusEvent");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(300, 300, 500, 350);
		
		this.add(new Panel());
		this.setVisible(true);
	}
}