package Eventos.EventosVentana;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class VentanaEventosVentanaClaseAdaptadora extends JFrame{

	public VentanaEventosVentanaClaseAdaptadora(){
		super("Ventana Respondiendo con clase adaptadora");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setBounds(300,300,500,350);
		this.addWindowListener(new Oyente());
		}

	class Oyente extends WindowAdapter{
		public void windowActivated(WindowEvent e){
			System.out.println("Ventana activada");
		}
		
		public void windowIconified(WindowEvent e){
			System.out.println("Ventana minimizada");
		}
	}	
}