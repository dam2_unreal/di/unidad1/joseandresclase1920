package Eventos.EventosVentana;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class VentanaEventosVentana extends JFrame implements WindowListener{

	public VentanaEventosVentana(){
		super("Ventana Respondiendo");
		this.setBounds(300,300,500,350);
		this.addWindowListener(this);
	}
	
	@Override
	public void windowActivated(WindowEvent e){
		System.out.println("Ventana activada");
	}

	@Override
	public void windowOpened(WindowEvent e) {
		System.out.println("Ventana abriendose");	
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.out.println("Ventana cerrada");		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.out.println("Ventana cerrandose");		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		System.out.println("Ventana Estado normal");		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		System.out.println("Ventana estado minimizado");		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		System.out.println("Ventana desactivada");		
	}

}