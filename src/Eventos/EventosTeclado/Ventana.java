package Eventos.EventosTeclado;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class Ventana extends JFrame {
    public Ventana() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBounds(300, 300, 500, 350);
        
        addKeyListener(new EventoDeTeclado());
        this.setVisible(true);
    }
}

class EventoDeTeclado implements KeyListener {
	
    public void keyPressed(KeyEvent e) {
        System.out.println(e.getKeyCode()); //Codigo
        System.out.println(e.getKeyChar()); //Letra
    }

    public void keyReleased(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}
}