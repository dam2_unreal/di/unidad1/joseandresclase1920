package Eventos.EventosAccionTeclado;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * @author Jose Andres Sanchez Perez
 * 29 oct. 2019
 */

public class Panel extends JPanel {
	
	ColorFondo colorAmarillo, colorAzul, colorRojo;
	JButton botonAmarillo, botonAzul, botonRojo;
	InputMap mapaEntrada;
	KeyStroke teclaAzul, teclaAmarillo, teclaRojo;
	ActionMap mapaAccion;
	
	public Panel() {
		InstanciarColores();
		InstanciarObjetosJButton();		
		PonerTextoEnBotones();
		BlindeoConAlt();
		TextoAlPasarElRaton();		
		BlindeoConCtrl();
		MapaEntrada();
		MapaAccion();		
		AnyadirAlPanel();
	}
	
	//------------------------------------------------------------------------------------------

	protected void InstanciarColores() {
		colorAmarillo = new ColorFondo(Color.YELLOW);
		colorAzul = new ColorFondo(Color.BLUE);
		colorRojo = new ColorFondo(Color.RED);
	}

	protected void InstanciarObjetosJButton() {
		botonAzul = new JButton(colorAzul);
		botonAmarillo = new JButton(colorAmarillo);
		botonRojo = new JButton(colorRojo);
	}
	
	protected void PonerTextoEnBotones() {
		botonAzul.setText("AZUL");
		botonAmarillo.setText("AMARILLO");
		botonRojo.setText("ROJO");
	}

	/**
	 * Con esto podemos blinder el boton con ALT + Cualquier tecla
	 */
	protected void BlindeoConAlt() {
		botonAzul.setMnemonic(KeyEvent.VK_A);
		botonAmarillo.setMnemonic(KeyEvent.VK_M);
		botonRojo.setMnemonic(KeyEvent.VK_R);
	}

	/**
	 * Informacion que sale al pasar el raton por el boton
	 */
	protected void TextoAlPasarElRaton() {
		botonAzul.setToolTipText("Pone el fondo Azul");
		botonAmarillo.setToolTipText("Pone el fondo Amarillo");
		botonRojo.setToolTipText("Pone el fondo Rojo");
	}

	protected void BlindeoConCtrl() {
		teclaAzul=KeyStroke.getKeyStroke("ctrl A");
		teclaAmarillo=KeyStroke.getKeyStroke("ctrl M");
		teclaRojo=KeyStroke.getKeyStroke("ctrl R");
	}

	protected void MapaEntrada() {	
		//getInputMap es un metodo de JComponent
		mapaEntrada = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		mapaEntrada.put(teclaAmarillo, "fondoAmarillo");
		mapaEntrada.put(teclaAzul, "fondoAzul");
		mapaEntrada.put(teclaRojo, "fondoRojo");
	}

	protected void MapaAccion() {
		//getActionMap es un metodo de JComponent
		mapaAccion=getActionMap();	
		mapaAccion.put("fondoAmarillo", colorAmarillo);
		mapaAccion.put("fondoAzul", colorAzul);
		mapaAccion.put("fondoRojo", colorRojo);
	}

	protected void AnyadirAlPanel() {
		this.add(botonAzul);
		this.add(botonAmarillo);
		this.add(botonRojo);
	}
	
	//------------------------------------------------------------------------------------------
	
	private class ColorFondo extends AbstractAction{
		private Color colorFondo;
		
		public ColorFondo(Color c) {
			colorFondo=c;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			setBackground(colorFondo);
		}
	}			
}