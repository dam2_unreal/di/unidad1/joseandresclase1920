package Eventos.EventosMetodos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelMetodo1 extends JPanel {
	
	private JButton btnRojo, btnAzul, btnAmarillo;
	private ColorFondo fondoAzul, fondoRojo, fondoAmarillo;
	
	public PanelMetodo1(){
		btnRojo = new JButton("Rojo");
		btnAzul = new JButton("Azul");
		btnAmarillo = new JButton("Amarillo");
		
		fondoAzul = new ColorFondo(Color.BLUE);
		btnAzul.addActionListener(fondoAzul);
		btnRojo.addActionListener(new ColorFondo(Color.RED));
		btnAmarillo.addActionListener(new ColorFondo(Color.YELLOW));
		
		this.add(btnRojo);
		this.add(btnAzul);
		this.add(btnAmarillo);
	}
	
	private class ColorFondo implements ActionListener{
		private Color colorFondo;		
		public ColorFondo(Color c){this.colorFondo = c;}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			setBackground(colorFondo);
		}	
	}
}
