package Eventos.EventosMetodos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelMetodo2 extends JPanel implements ActionListener{
	
	private JButton btnRojo, btnAzul, btnAmarillo;
	
	public PanelMetodo2(){
		btnRojo = new JButton("Rojo");
		btnAzul = new JButton("Azul");
		btnAmarillo = new JButton("Amarillo");
		
		btnAzul.addActionListener(this);
		btnRojo.addActionListener(this);
		btnAmarillo.addActionListener(this);
		
		this.add(btnRojo);
		this.add(btnAzul);
		this.add(btnAmarillo);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == btnRojo) this.setBackground(Color.RED);
		if(e.getSource() == btnAmarillo) this.setBackground(Color.YELLOW);
		if(e.getSource() == btnAzul) this.setBackground(Color.BLUE);
		
	}	
}