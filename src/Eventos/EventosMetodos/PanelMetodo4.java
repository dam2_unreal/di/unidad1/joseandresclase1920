package Eventos.EventosMetodos;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelMetodo4 extends JPanel {

	private JButton btnRojo, btnAzul, btnAmarillo;
	
	public PanelMetodo4(){
		btnRojo = new JButton("Rojo");
		btnAzul = new JButton("Azul");
		btnAmarillo = new JButton("Amarillo");
		
		btnAzul.addActionListener(e -> Accion1());
		btnRojo.addActionListener(e -> setBackground(Color.RED));
		btnAmarillo.addActionListener(e -> setBackground(Color.YELLOW));
		
		this.add(btnRojo);
		this.add(btnAzul);
		this.add(btnAmarillo);
	}
	
	public void Accion1(){ 
		//No haria falta ahora pero cuando tengamos mas de una linea sera necesario
		setBackground(Color.BLUE);
	}
}