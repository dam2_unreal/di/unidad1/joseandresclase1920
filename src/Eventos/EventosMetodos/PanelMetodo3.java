package Eventos.EventosMetodos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelMetodo3 extends JPanel{

private JButton btnRojo, btnAzul, btnAmarillo;
	
	public PanelMetodo3(){
		btnRojo = new JButton("Rojo");
		btnAzul = new JButton("Azul");
		btnAmarillo = new JButton("Amarillo");
		
		//Usando clases anonimas
		btnAzul.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				setBackground(Color.BLUE);				
			}
		});
		
		btnRojo.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				setBackground(Color.RED);
			}
		});
		
		btnAmarillo.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent e) {
				setBackground(Color.YELLOW);
			}
		});
		
		this.add(btnRojo);
		this.add(btnAzul);
		this.add(btnAmarillo);
	}
	
}