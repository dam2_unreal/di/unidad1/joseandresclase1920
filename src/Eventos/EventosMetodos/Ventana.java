package Eventos.EventosMetodos;

import javax.swing.JFrame;

public class Ventana extends JFrame{
	
	public Ventana(){
		super("Eventos Metodos");	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 50, 700, 600);
		
//		this.add(new PanelMetodo1());
//		this.add(new PanelMetodo2());
//		this.add(new PanelMetodo3());
		this.add(new PanelMetodo4());
		this.setVisible(true);
	}
	
}