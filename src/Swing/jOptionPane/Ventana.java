package Swing.jOptionPane;

import javax.swing.JFrame;

public class Ventana extends JFrame{
	
	private PanelShowMessageDialog psmd;
	private PanelShowInputDialog psid;
	private PanelShowConfirmDialog pscd;
	private PanelShowOptionDialog psod;

	public Ventana(){
		super("JOptionPane");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 50, 700, 600);
		
		//psmd =  new PanelShowMessageDialog();
		//psid =  new PanelShowInputDialog();
		//pscd =  new PanelShowConfirmDialog();
		psod =  new PanelShowOptionDialog();
		
		//this.add(psmd);
		//this.add(psid);
		//this.add(pscd);
		this.add(psod);
	}
	
}