package Swing.Contenedores.JFrameJDialog2;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * @author Jose Andres Sanchez Perez
 * 31 oct. 2019
 */

public class Dialogo extends JDialog{
	
	private Ventana miVentana;

	public Dialogo(Ventana miVentana, boolean modal) {
		super(miVentana, modal);
		this.setSize(400, 400);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		
		this.setVisible(true);
	}
	
}