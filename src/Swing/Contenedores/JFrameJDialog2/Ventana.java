package Swing.Contenedores.JFrameJDialog2;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * @author Jose Andres Sanchez Perez
 * 31 oct. 2019
 */

public class Ventana extends JFrame{

	private JButton miBoton;
	private Ventana miVentana;
	private Dialogo miDialogo;

	public Ventana() {
		super("Ventana");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(300, 200);
		
		iniciarComponentes();
		
		this.setVisible(true);
	}
	
	//---------------------------GETTERS Y SETTERS--------------------------------
	
	public void setMiVentana(Ventana miVentana) {
		this.miVentana = miVentana;
	}
	
	//---------------------------COMPONENETES-------------------------------------
	
	public void iniciarComponentes(){
		miBoton = new JButton("Abrir Dialogo");
		miBoton.addActionListener(e -> {
			miDialogo = new Dialogo(miVentana, true);
		});
		
		add(miBoton);
	}
	
}