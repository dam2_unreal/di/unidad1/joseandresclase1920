package Swing.Contenedores.Ej1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class VentanaPrincipal extends JFrame implements ActionListener {
	private JPanel miPanel;
	private JButton botonCambiar;
	private JLabel labelTitulo;
	private VentanaPrincipal miVentanaPrincipal;
	
	public VentanaPrincipal() {
		setTitle("JFrame VentanaPrincipal");
		setSize(300, 180);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		iniciarComponentes();
	}
	
	/**
	* @param miVentana Enviamos una instancia de la ventana principal
	*/
	public void setVentanaPrincipal(VentanaPrincipal miVentana) {
		miVentanaPrincipal = miVentana;
	}
	
	private void iniciarComponentes() {
		miPanel = new JPanel();

		botonCambiar = new JButton();
		botonCambiar.setText("Iniciar");
		botonCambiar.setBounds(100, 80, 80, 23);
		botonCambiar.addActionListener(this);
		
		labelTitulo = new JLabel();
		labelTitulo.setText("VENTANA PRINCIPAL");
		labelTitulo.setBounds(80, 20, 180, 23);
		
		miPanel.add(labelTitulo);
		miPanel.add(botonCambiar);
		add(miPanel);
	}
	
	@Override
	public void actionPerformed(ActionEvent evento) {
		if (evento.getSource() == botonCambiar) {
			VentanaConfirmacion miVentanaConfirmacion = new VentanaConfirmacion(miVentanaPrincipal, true);
			miVentanaConfirmacion.setVisible(true);
		}
	}
}