package Swing.Contenedores;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

public class DosVentanas {

	private JFrame ventanaPrincipal;
	private JDialog ventanaSecundaria;
	private JButton boton;
	private JLabel etiqueta;
	
	public DosVentanas(){
		inicializarComponentes();	
		empaquetarVentanas();	
		establecerTamaņoVentanas();
		Listener();
		
		ventanaPrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		ventanaPrincipal.setVisible(true);
	}
	
	//-----------------------------------------------------------------------------------------------------

	/**
	 * @throws HeadlessException
	 */
	private void inicializarComponentes() throws HeadlessException {
		ventanaPrincipal = new JFrame("Ventana principal");
		ventanaSecundaria = new JDialog(ventanaPrincipal,true);
		boton = new JButton("Abre secundaria");
		etiqueta = new JLabel("Hola");
	}

	private void empaquetarVentanas() {
		ventanaPrincipal.getContentPane().add(boton);
		ventanaSecundaria.getContentPane().add(etiqueta);
		
		ventanaPrincipal.pack();
		ventanaSecundaria.pack();
	}

	private void establecerTamaņoVentanas() {
		ventanaPrincipal.setSize(300, 300);	
		ventanaSecundaria.setSize(300, 300);
	}

	private void Listener() {
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				ventanaPrincipal.setVisible(false);
				ventanaSecundaria.setVisible(true);
			}
		
		});
		
//		ventanaSecundaria.addWindowListener(new WindowAdapter() {
//			public void windowClosing(WindowEvent e) {
//				ventanaPrincipal.setVisible(true);
//				ventanaSecundaria.setVisible(false);
//			}
//		
//			public void windowClosed(WindowEvent e) {
//				ventanaPrincipal.setVisible(true);
//				ventanaSecundaria.setVisible(false);
//			}
//		});
	}
	
	//-----------------------------------------------------------------------------------------------------
	
	public static void main(String[] args) {
		new DosVentanas();
	}
}