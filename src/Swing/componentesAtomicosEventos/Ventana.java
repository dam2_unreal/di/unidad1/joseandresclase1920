package Swing.componentesAtomicosEventos;

import javax.swing.JFrame;

public class Ventana extends JFrame{
	

	public Ventana(){
		super("Componentes atomicos");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 50, 700, 600);
		
		this.add(new Panel());
	}
	
}