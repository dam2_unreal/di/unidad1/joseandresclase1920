package Swing.jSpinnerVarios;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

public class Panel extends JPanel {
	
	private JSpinner jsp1, jsp2, jsp3, jsp4, jsp5;
	private JLabel jb;
	private JTextField jtf;
	private String[] lista = {"Huercal Overa","Antas","Garrucha","Sorbas","Lorca"};
	private String[] listaF = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	private Dimension d = new Dimension(110,20);
	
	public Panel(){	
		
		jb = new JLabel("JSpinner");
		jb.setFont(new Font("Tahoma",Font.BOLD,16));
		jb.setForeground(Color.RED);
		
		jsp1 = new JSpinner();
		jsp2 = new JSpinner(new SpinnerDateModel());
		Arrays.sort(lista);
		
		jsp3 = new JSpinner(new SpinnerListModel(lista));
		jsp3.setPreferredSize(d);
		
		jsp4 = new JSpinner(new SpinnerListModel(listaF));
		jsp4.setPreferredSize(d);
		
		jsp5 = new JSpinner(new SpinnerNumberModel(5,0,10,2));
		
		jtf = new JTextField("F");
		jtf.setPreferredSize(d);
			
		this.add(jb);
		this.add(jsp1);
		this.add(jsp2);
		this.add(jsp3);
		this.add(jsp4);
		this.add(jsp5);	
		this.add(jtf);

	}

}
