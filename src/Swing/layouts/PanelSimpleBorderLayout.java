package Swing.layouts;

import java.awt.*;
import javax.swing.*;

public class PanelSimpleBorderLayout extends JPanel {
	public PanelSimpleBorderLayout() {
		this.setLayout(new BorderLayout());
		this.add(new JLabel("Norte", SwingConstants.CENTER), BorderLayout.SOUTH);
		this.add(new JLabel("Sur", SwingConstants.CENTER), BorderLayout.SOUTH);
		this.add(new JLabel("Centro", SwingConstants.CENTER), BorderLayout.CENTER);
		this.add(new JLabel("Oeste", SwingConstants.CENTER), BorderLayout.WEST);
		this.add(new JLabel("Este", SwingConstants.CENTER), BorderLayout.EAST);
	}
}