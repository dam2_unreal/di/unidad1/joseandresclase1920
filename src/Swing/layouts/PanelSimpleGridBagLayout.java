package Swing.layouts;

import java.awt.*;
import javax.swing.*;

class PanelSimpleGridBagLayout extends JPanel {
	private GridBagLayout layout; // layout of this frame
	private GridBagConstraints constraints; // constraints of this layout
// set up GUI

	public PanelSimpleGridBagLayout() {
		layout = new GridBagLayout();
		this.setLayout(layout); // set frame layout
		constraints = new GridBagConstraints(); // instantiate constraints
//create GUI components
		JTextArea textArea1 = new JTextArea("TextArea1", 5, 10);
		JTextArea textArea2 = new JTextArea("TextArea2", 2, 2);
		String[] names = { "Iron", "Steel", "Brass" };
		JComboBox<String> comboBox = new JComboBox<String>(names);
		JTextField textField = new JTextField("TextField");
		JButton button1 = new JButton("Button 1");
		JButton button2 = new JButton("Button 2");
		JButton button3 = new JButton("Button 3");
//weightx. La cantidad de espacio adicional para asignar horizontalmente.
//La cuadr�cula la ranura puede hacerse m�s ancha cuando se dispone de espacio adicional.
//weighty.La cantidad de espacio extra para asignar verticalmente.
//La ranura de la rejilla puede llegar a ser m�s alto cuando el espacio adicional est� disponible
//weightx and weighty for textArea1 are both 0: the default 
//anchor. especifica la posici�n relativa del componente en un �rea que no se llena
//anchor for all components is CENTER: the default
//fill. Cambia el tama�o del componente en la direcci�n especificada (NONE, HORIZONTAL, VERTICAL, AMBOS)
//cuando el �rea de visualizaci�n es mayor que el componente.
		constraints.fill = GridBagConstraints.BOTH;
		addComponent(textArea1, 0, 0, 1, 3);
//weightx and weighty for button1 are both 0: the default
//fill is HORIZONTAL
		constraints.fill = GridBagConstraints.HORIZONTAL;
		this.addComponent(button1, 0, 1, 2, 1);
//weightx and weighty for comboBox are both 0: the default
		this.addComponent(comboBox, 2, 1, 2, 1);
//button2
		constraints.weightx = 1000; // puede crecer m�s ancho
		constraints.weighty = 1; // puede crecer m�s alto
		constraints.fill = GridBagConstraints.BOTH;
		this.addComponent(button2, 1, 1, 1, 1);
//fill is BOTH for button3
		constraints.weightx = 0;
		constraints.weighty = 0;
		this.addComponent(button3, 1, 2, 1, 1);
//weightx and weighty for textField are both 0, fill is BOTH
		this.addComponent(textField, 3, 0, 2, 1);
//weightx and weighty for textArea2 are both 0, fill is BOTH
		this.addComponent(textArea2, 3, 2, 1, 1);
	} // end GridBagFrame constructor
//method to set constraints on

	private void addComponent(Component component, int row, int column, int width, int height) {
		constraints.gridx = column;
// set gridx, La columna en la que se colocar� el componente.
		constraints.gridy = row;
// set gridy, La fila en la que se colocar� el componente.
		constraints.gridwidth = width;
// set gridwidth, El n�mero de columnas que ocupa el componente.
		constraints.gridheight = height;
// set gridheight, El n�mero de filas que ocupa el componente.
		layout.setConstraints(component, constraints); // set constraints
		this.add(component); // add component
	} // end method addComponent
} // end class GridBagFrame