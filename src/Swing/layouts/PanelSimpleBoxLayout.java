package Swing.layouts;

import java.awt.*;
import javax.swing.*;

public class PanelSimpleBoxLayout extends JPanel {
	public PanelSimpleBoxLayout() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(new JLabel("Primera"));
		this.add(new JLabel("Segunda"));
		this.add(new JLabel("Tercera"));
		this.add(new JLabel("Cuarta"));
		this.add(new JLabel("Quinta"));
		this.add(new JLabel("Sexta"));
	}
}
