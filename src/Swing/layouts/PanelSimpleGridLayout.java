package Swing.layouts;

import java.awt.*;
import javax.swing.*;

public class PanelSimpleGridLayout extends JPanel {
	public PanelSimpleGridLayout() {
		this.setLayout(new GridLayout(3, 2, 15, 15));
// this.add(new JLabel("Primera"));
// this.add(new JLabel("Segunda"));
// this.add(new JLabel("Tercera"));
// this.add(new JLabel("Cuarta"));
// this.add(new JLabel("Quinta"));
// this.add(new JLabel("Sexta"));

		this.add(new JButton("Primera"));
		this.add(new JButton("Segunda"));
		this.add(new JButton("Tercera"));
		this.add(new JButton("Cuarta"));
		this.add(new JButton("Quinta"));
		this.add(new JButton("Sexta"));
	}
}