package Swing.componentesAtomicos;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.BevelBorder;

public class Panel extends JPanel {
	
	private JLabel jbTitulo, jbEtiqueta, etqBoton, etqCheckbox, etqRadioButton, etqTongleButton, etqIcon, etqIco2;
	private JButton btn, btnImg;
	private JCheckBox jcb1, jcb2;
	private JRadioButton jrb1, jrb2;
	private ButtonGroup grupoRadios;
	private JToggleButton jtb;
	private JComboBox<String> jComboBox;
	private JSeparator separadorVertical, separardorHorizontal;
	private JSlider deslizador;
	private JSpinner spinner;
	private JProgressBar barra;
	private MiIcon ico;
	private ImageIcon imgIco, imgIco2;
	private String[] opciones = {"A","B","C"};
	
	public Panel(){	
		this.setLayout(null);
		jbTitulo = new JLabel();
		jbTitulo.setText("Componentes atomicos");
		jbTitulo.setFont(new Font("Arial", Font.BOLD, 18));
		jbTitulo.setForeground(Color.RED);
		jbTitulo.setBounds(180, 10, 380, 40);
		
		jbEtiqueta = new JLabel("Esto es un Label o Etiqueta");
		jbEtiqueta.setBounds(20, 50, 280, 23);
		jbEtiqueta.setOpaque(true);
		jbEtiqueta.setBackground(new Color(100, 150, 50));
		
		etqBoton = new JLabel("JButon: ");
		etqBoton.setBounds(20, 80, 280, 34);
		
		etqCheckbox = new JLabel("Checkbox: ");
		etqCheckbox.setBounds(20, 110, 280, 34);
		
		etqRadioButton = new JLabel("RadioButton: ");
		etqRadioButton.setBounds(20, 140, 280, 34);
		
		etqTongleButton = new JLabel("TongleButton: ");
		etqTongleButton.setBounds(20, 170, 280, 34);
		
		ico = new MiIcon();
		etqIcon = new JLabel(ico);
		etqIcon.setBounds(10, 400, 60, 70);
		
		imgIco = new ImageIcon(getClass().getResource("../Recursos/Icon.png"));
		etqIco2 = new JLabel(imgIco, SwingConstants.CENTER);
		etqIco2.setBounds(0, 0, 630, 500);
		
		btn = new JButton("Boton");
		//btn.setBorder(null);
		btn.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		btn.setBorder(BorderFactory.createDashedBorder(Color.MAGENTA));
		btn.setBackground(Color.ORANGE);
		btn.setBounds(80, 80, 80, 23);
		
		imgIco2 = new ImageIcon(getClass().getResource("../Recursos/Icon.png"));
		btnImg = new JButton(imgIco2);
		btnImg.setBounds(550, 100, 250, 150);
		
		jcb1 = new JCheckBox("Check1");
		jcb1.setBounds(100, 110, 80, 23);
		
		jcb2 = new JCheckBox("Check2",true);
		jcb2.setBounds(180, 110, 80, 23);
		
		jrb1 = new JRadioButton("Radio1",true);
		jrb1.setBounds(100, 140, 80, 23);
		
		jrb2 = new JRadioButton("Radio2");
		jrb2.setBounds(190, 140, 80, 23);
		
		grupoRadios = new ButtonGroup();
		grupoRadios.add(jrb1);
		grupoRadios.add(jrb2);
		
		jtb = new JToggleButton("Activar",true);
		jtb.setBounds(120, 180, 80, 23);
		
		jComboBox = new JComboBox<String>(opciones);
		jComboBox.setBounds(430, 50, 100, 23);
		
		separadorVertical = new JSeparator(SwingConstants.VERTICAL);
		separadorVertical.setBounds(300, 60, 5, 155);
		
		separardorHorizontal = new JSeparator(SwingConstants.HORIZONTAL);
		separardorHorizontal.setBounds(430, 92, 200, 100);
		
		deslizador = new JSlider(SwingConstants.HORIZONTAL,0,100,50);
		deslizador.setMajorTickSpacing(25);
		deslizador.setMinorTickSpacing(5);
		deslizador.setPaintTicks(true); //Marcas
		deslizador.setPaintLabels(true); //Numeros
		deslizador.setBounds(430, 130, 200, 70);
		
		spinner = new JSpinner();
		spinner.setBounds(450, 210, 50, 30);
		
		barra = new JProgressBar(0,150);
		barra.setBounds(450, 260, 110, 20);
		barra.setValue(130);
		
		//A�adir componentes
		this.add(jbTitulo);
		this.add(jbEtiqueta);
		this.add(etqBoton);
		this.add(etqCheckbox);
		this.add(etqRadioButton);
		this.add(etqTongleButton);
		//this.add(etqIcon);
		//this.add(etqIco2);
		this.add(btn);
		//this.add(btnImg);
		this.add(jcb1);
		this.add(jcb2);
		this.add(jrb1);
		this.add(jrb2);
		this.add(jtb);
		this.add(jComboBox);
		this.add(separadorVertical);
		this.add(separardorHorizontal);
		this.add(deslizador);
		this.add(spinner);
		this.add(barra);
	}

}
